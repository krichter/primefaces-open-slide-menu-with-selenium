package de.richtercloud.primefaces.open.slide.menu.with.selenium;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

@RunWith(Arquillian.class)
public class FunctionalTest {

    @Deployment(testable = false)
    public static Archive<?> createDeployment0() throws ParserConfigurationException,
            SAXException,
            IOException,
            XPathExpressionException,
            TransformerConfigurationException,
            TransformerException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document webXmlDocument = documentBuilder.parse(new File("src/main/webapp/WEB-INF/web.xml"));
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xPath = xPathfactory.newXPath();

        XPathExpression expr = xPath.compile("//*[local-name()='web-app']");
        Node webAppDataSourceNode = (Node) expr.evaluate(webXmlDocument,
                XPathConstants.NODE);
        Element listenerNode = webXmlDocument.createElement("listener");
        Element listenerClassNode = webXmlDocument.createElement("listener-class");
        listenerClassNode.setTextContent("com.sun.faces.config.ConfigureListener");
        listenerNode.appendChild(listenerClassNode);
        webAppDataSourceNode.appendChild(listenerNode);

        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(webXmlDocument), new StreamResult(writer));
        String webXml = writer.toString();
        System.out.println("web.xml content: " + webXml);
        StringAsset webXmlAsset = new StringAsset(webXml);
        WebArchive archive = ShrinkWrap.create(WebArchive.class)
                .setWebXML(webXmlAsset)
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/glassfish-web.xml"))
                .addAsWebResource(new File("src/main/webapp/index.xhtml"))
                .addClass(BackingBean0.class)
                .addAsLibrary(Maven.configureResolver().resolve("org.primefaces:primefaces:6.2").withTransitivity().asSingle(JavaArchive.class))
                ;
        return archive;
    }

    @Drone
    private WebDriver webDriver;
    @ArquillianResource
    private URL deploymentUrl;
    @FindBy(id = "mainForm:menuTrigger")
    private WebElement menuTrigger;
    @FindBy(id = "mainForm:menuItem1")
    private WebElement menuItem1;
    @FindBy(id = "mainForm:menuHideLabel")
    private WebElement menuHideLabel;

    @Test
    public void testSomeMethod() throws IOException,
            InterruptedException {
        webDriver.get(deploymentUrl.toExternalForm()+"/index.xhtml");
        menuTrigger.click();
        new WebDriverWait(webDriver, 5).until(ExpectedConditions.visibilityOf(menuItem1));
        menuHideLabel.click();
        new WebDriverWait(webDriver, 5).until(ExpectedConditions.invisibilityOf(menuItem1));
        menuTrigger.click();
        new WebDriverWait(webDriver, 5).until(ExpectedConditions.visibilityOf(menuItem1));
    }
}
