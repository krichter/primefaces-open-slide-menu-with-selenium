package de.richtercloud.primefaces.open.slide.menu.with.selenium;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named
@ViewScoped
public class BackingBean0 implements Serializable {
    private static final long serialVersionUID = 1L;
    private String background = "";

    public void listener0() {
        System.out.println("listener0");
        background = "green";
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }
}
